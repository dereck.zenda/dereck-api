# API

[API App](https://api.dereckzenda.io) is a RESTful Web Service build with open source tools [Apache Maven](https://maven.apache.org/guides/index.html) and [Spring RESTful Web Service](https://spring.io/guides/gs/rest-service/) with a PostgreSQL database. This is designed to be run on kubernetes and deployed with [helm](https://helm.sh/). The api consists of two components , the api docker image build as part of the ci/cd pipeline and the database image using official postgreSQL docker images.

### Prerequisites
- Kubernetes 1.10+ 
- PV provisioner support in the underlying infrastructure (Only when persisting data)
- helm

### API Components
The API consists of the following components
 - api-exercise - this contains the base application
 - postgres - database for the app


## Deployment
The deployment of the application is done through helm charts in   `'kubernetes/helm/api-exercise'` through a defined gitlab ci/cd as defined in the `.gitlab.ci.yml`. This can also be done separately. Please check `'kubernetes/helm/api-exercise/README.md'` on how to do that.


### Installing the Chart
To install the chart with the release name `my-release`:

```bash
$ cd helm/api-exercise
$ helm dep update
$ helm install --name my-release .
```
The command deploys api and postgreSQL on the Kubernetes cluster, an extra parameter `--namespace` can be specified to install in a particular namespace.

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```
The command removes all the Kubernetes components associated with the chart and deletes the release.

### Configuration
The following environment variables are defined for the API

-	`DB_HOST=...` (defaults to the service in kubernertes)
-	`DB_USER=...` (defaults to "root")
-	`DB_PASSWORD=...` 
-	`DB_NAME=...` (defaults to "myapp")
-   `JAVA_OPTS=-Dspring.config.location=/home/java/app/application.properties` 

## CI/CD Pipeline

The `'.gitlab-ci.yml'`defines the details of the pipeline to be used with gitlab.

Once the app is deployed, it should be accessible through https://api.dereckzenda.io

## Running the API

All calls can be made as defined as described in [API.md](./API.md). Examples:

#### GET /people
```
curl -v localhost:8080/people | jq .
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0* About to connect() to localhost port 8080 (#0)
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /people HTTP/1.1
> User-Agent: curl/7.29.0
> Host: localhost:8080
> Accept: */*
> 
< HTTP/1.1 200 
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Thu, 30 Jan 2020 20:48:45 GMT
< 
{ [data not shown]
100   416    0   416    0     0  24525      0 --:--:-- --:--:-- --:--:-- 26000
* Connection #0 to host localhost left intact
[
  {
    "uuid": "0ca9eb0d-c525-4b68-94d0-898b070826f1",
    "survived": true,
    "passengerClass": 3,
    "name": "Mr. Owen Harris Braund",
    "sex": "male",
    "age": 22,
    "siblingsOrSpousesAboard": 1,
    "parentsOrChildrenAboard": 0,
    "fare": 7.25
  },
  {
    "uuid": "4b5836fa-9efd-4d04-a51b-bb22597c4c4c",
    "survived": false,
    "passengerClass": 3,
    "name": "Mrs. Ruru Desmond Braund",
    "sex": "female",
    "age": 22,
    "siblingsOrSpousesAboard": 1,
    "parentsOrChildrenAboard": 0,
    "fare": 7.25
  }
]
```
#### POST /people

```
curl -X POST localhost:8080/people -H 'Content-type:application/json' -d '{ "survived": true, "passengerClass": 3, "name": "Mr. Dereck bra", "sex": "male", "age": 22, "siblingsOrSpousesAboard": 1, "parentsOrChildrenAboard":0, "fare":7.25}' | jq .
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   356    0   194  100   162  11351   9478 --:--:-- --:--:-- --:--:-- 11411
{
  "uuid": "82a13f90-25e5-4a52-a1d1-539cafa7a7c0",
  "survived": true,
  "passengerClass": 3,
  "name": "Mr. Dereck bra",
  "sex": "male",
  "age": 22,
  "siblingsOrSpousesAboard": 1,
  "parentsOrChildrenAboard": 0,
  "fare": 7.25
}
```

#### GET /people/{uuid}

```
curl -v localhost:8080/people/0ca9eb0d-c525-4b68-94d0-898b070826f1 | jq .
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0* About to connect() to localhost port 8080 (#0)
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /people/0ca9eb0d-c525-4b68-94d0-898b070826f1 HTTP/1.1
> User-Agent: curl/7.29.0
> Host: localhost:8080
> Accept: */*
> 
< HTTP/1.1 200 
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Thu, 30 Jan 2020 20:56:22 GMT
< 
{ [data not shown]
100   204    0   204    0     0   8734      0 --:--:-- --:--:-- --:--:--  8869
* Connection #0 to host localhost left intact
{
  "uuid": "0ca9eb0d-c525-4b68-94d0-898b070826f1",
  "survived": true,
  "passengerClass": 3,
  "name": "Mr. Owen Harris Braund",
  "sex": "male",
  "age": 22,
  "siblingsOrSpousesAboard": 1,
  "parentsOrChildrenAboard": 0,
  "fare": 7.25
}
```

#### DELETE /people/{uuid}
```
 curl -X DELETE localhost:8080/people/0ca9eb0d-c525-4b68-94d0-898b070826f1
```

#### PUT /people/{uuid}
```
curl -X PUT localhost:8080/people/b350ed78-7869-40e4-ba7b-9880b224867c -H 'Content-type:application/json' -d '{ "survived": true, "passengerClass": 3, "name": "Mr. jjj", "sex": "male", "age": 22, "siblingsOrSpousesAboard": 1, "parentsOrChildrenAboard":0, "fare":7.25}' | jq .
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   346    0   189  100   157   2864   2379 --:--:-- --:--:-- --:--:--  2907
{
  "uuid": "5a46ca95-fda9-4564-8d03-f6741898750e",
  "survived": true,
  "passengerClass": 3,
  "name": "Mr. jjj",
  "sex": "male",
  "age": 22,
  "siblingsOrSpousesAboard": 1,
  "parentsOrChildrenAboard": 0,
  "fare": 7.25
}
```

## Swagger