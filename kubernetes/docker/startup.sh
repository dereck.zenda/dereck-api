#!/bin/sh

echo 'Starting API App'

API_JAR=`find /home/java/app -name '*.jar' | paste -sd ":" -`
exec /usr/bin/java  $JAVA_OPTS -cp /home/java/app/:$API_JAR org.springframework.boot.loader.JarLauncher $APP_ARGS
