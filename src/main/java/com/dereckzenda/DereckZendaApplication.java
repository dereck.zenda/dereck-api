package com.dereckzenda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class DereckZendaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DereckZendaApplication.class, args);
	}

}
