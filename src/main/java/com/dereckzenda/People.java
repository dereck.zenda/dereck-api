package com.dereckzenda;

import lombok.Data;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Data
@Entity

class People {
	@Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "uuid", updatable = false, nullable = false)
    private UUID uuid;
	private boolean survived;
	private int passengerClass;
	private String name;
	private String sex;
	private int age;
	private int siblingsOrSpousesAboard;
	private int parentsOrChildrenAboard;
	private double fare;
	
	People(){}
	
	People(boolean survived,int passengerClass,String name,String sex,int age,int siblingsOrSpousesAboard,int parentsOrChildrenAboard, double fare){
		this.survived=survived;
		this.passengerClass=passengerClass;
		this.name=name;
		this.sex=sex;
		this.age=age;
		this.siblingsOrSpousesAboard=siblingsOrSpousesAboard;
		this.parentsOrChildrenAboard=parentsOrChildrenAboard;
		this.fare=fare;
	}
	
	public boolean getSurvived() {
		return this.survived;
	}
	
	public int getPassengerClass() {
		return this.passengerClass;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getSex() {
		return this.sex;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public int getSiblingsOrSpousesAboard() {
		return this.siblingsOrSpousesAboard;
	}
	
	public int getParentsOrChildrenAboard() {
		return this.parentsOrChildrenAboard;
	}
	
	public double getFare() {
		return this.fare;
	}
	
	public void setSurvived( boolean survived) {
		this.survived=survived;
	}
	
	public void setPassengerClass(int passengerClass) {
		this.passengerClass=passengerClass;
	}
	
	public void setName(String name) {
		this.name=name;
	}
	
	public void setSex(String sex) {
		this.sex=sex;
	}
	
	public void setAge(int age) {
		this.age=age;
	}
	
	public void setSiblingsOrSpousesAboard(int siblingsOrSpousesAboard) {
		this.siblingsOrSpousesAboard=siblingsOrSpousesAboard;
	}
	
	public void setParentsOrChildrenAboard(int parentsOrChildrenAboard) {
		this.parentsOrChildrenAboard=parentsOrChildrenAboard;
	}
	
	public void setFare(double fare) {
		this.fare=fare;
	}

	public void setUUID(UUID uuid) {
		this.uuid=uuid;
	}
	
	public UUID getUUID() {
		return this.uuid;
	}
}

