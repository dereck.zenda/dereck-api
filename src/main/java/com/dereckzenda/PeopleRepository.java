package com.dereckzenda;

import java.util.UUID;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
interface PeopleRepository extends JpaRepository<People, UUID> {
}
